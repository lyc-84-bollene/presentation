# Arts plastiques

Une présentation par Mme Drevet

Cliquer sur les images pour voir la description complète.

## Option classe de seconde

[![option](option.png){width=100%}](option.pdf)

## Spécialité en première et terminale

[![spé](spé.png){width=100%}](arts-pla.pdf)

--8<-- "includes/abbreviations.md"
