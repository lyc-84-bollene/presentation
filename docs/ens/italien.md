# Italien

Il s'agit d'un enseignement dans la continuité du collège ciblant les compétences comme la compréhension de l’oral et de l’écrit et l'expression écrite et orale.

## Volume horaire

Les horaires en seconde sont de 2 h 30 par semaine.

## Programme

Les thèmes abordés sont les mêmes dans toutes les langues et suivent les programmes officiels (p. ex : famille, sport, art, ville...)

Les documents étudiés sont proposés sur des supports variés (p. ex. articles de journaux, chansons, films, reportages audio, extraits de textes littéraires, photos et autres supports iconographiques...)
Nous veillons à mettre tous les élèves à niveau en début de Seconde afin de leur permettre d'aborder les épreuves du bac qui débutent au milieu de la Première avec plus de sérénité.

Par ailleurs, les élèves volontaires ont la possibilité de participer aux projets divers comme les échanges, les voyages, les sorties, etc.

## Quelques photos

### Le Colisée

<p><a href="https://commons.wikimedia.org/wiki/File:Colosseo_2020.jpg#/media/Fichier:Colosseo_2020.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Colosseo_2020.jpg/1200px-Colosseo_2020.jpg" alt="Image illustrative de l’article Colisée"></a><br><a href="https://creativecommons.org/licenses/by-sa/4.0" title="Creative Commons Attribution-Share Alike 4.0">CC BY-SA 4.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=95579199">Lien</a></p>

### Arlequin

<p><a href="https://commons.wikimedia.org/wiki/File:Arlequin_-_commedia_dell%27arte.JPG#/media/Fichier:Arlequin_-_commedia_dell'arte.JPG"><img src="https://upload.wikimedia.org/wikipedia/commons/1/14/Arlequin_-_commedia_dell%27arte.JPG" alt="Statuette d'Arlequin en plâtre peint provenant du Théâtre Séraphin et conservée au musée Carnavalet, à Paris"></a><br><a href="https://creativecommons.org/licenses/by/3.0" title="Creative Commons Attribution 3.0">CC BY 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=2734723">Lien</a></p>

### Le drapeau d'Italie

<p><a href="https://commons.wikimedia.org/wiki/File:Flag_of_Italy.svg#/media/Fichier:Flag_of_Italy.svg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/1200px-Flag_of_Italy.svg.png" alt="Drapeau"></a><br> Domaine public, <a href="https://commons.wikimedia.org/w/index.php?curid=343615">Lien</a></p>

### _Cannoli siciliani_

<p><a href="https://commons.wikimedia.org/wiki/File:Cannoli_siciliani_(7472226896).jpg#/media/Fichier:Cannoli_siciliani_(7472226896).jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Cannoli_siciliani_%287472226896%29.jpg/1200px-Cannoli_siciliani_%287472226896%29.jpg" alt="Image illustrative de l’article Cannolo"></a><br><a href="https://creativecommons.org/licenses/by/2.0" title="Creative Commons Attribution 2.0">CC BY 2.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=25642817">Lien</a></p>

### Fromage _Parmigiano-Reggiano_

<p><a href="https://commons.wikimedia.org/wiki/File:Parmigiano-Reggiano_at_Eataly_in_Stockholm.jpg#/media/Fichier:Parmigiano-Reggiano_at_Eataly_in_Stockholm.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Parmigiano-Reggiano_at_Eataly_in_Stockholm.jpg/1200px-Parmigiano-Reggiano_at_Eataly_in_Stockholm.jpg" alt="Image dans Infobox."></a><br><a href="https://creativecommons.org/licenses/by-sa/4.0" title="Creative Commons Attribution-Share Alike 4.0">CC BY-SA 4.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=67227312">Lien</a></p>

### Pâtes italiennes

<p><a href="https://commons.wikimedia.org/wiki/File:P%C3%A2tes_alimentaires_10.jpg#/media/Fichier:Pâtes_alimentaires_10.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/P%C3%A2tes_alimentaires_10.jpg/1200px-P%C3%A2tes_alimentaires_10.jpg" alt="Pâtes alimentaires 10.jpg"></a><br><a href="https://creativecommons.org/licenses/by-sa/4.0" title="Creative Commons Attribution-Share Alike 4.0">CC BY-SA 4.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=63061894">Lien</a></p>


--8<-- "includes/abbreviations.md"
