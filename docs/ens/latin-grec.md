# Latin-grec

**ECLA** : Enseignement Conjoint Langues Anciennes, latin-grec

## Les horaires et la formation

C'est un enseignement de 3 heures par semaine qui est optionnel et qui peut être poursuivi jusqu'au baccalauréat en terminale. Il est évalué en contrôle continu et c'est la seule option à être valorisée par un coefficient 3 au baccalauréat.

## À qui s'adresse cette option ?

Parce que c'est une option qui s'adresse à tous, qu'on veuille faire des sciences ou de la littérature car il s'agit de développer la culture générale en revenant aux racines de notre culture occidentale, de découvrir l'origine des mots et donc d'enrichir son vocabulaire et de mieux comprendre le vocabulaire scientifique et littéraire formé sur les racines latines et grecques. Il s'agit aussi de découvrir la culture antique à la fois très proche et très différente de la nôtre qui en provient. De s'interroger sur la mythologie, de connaître l'histoire antique et de faire des liens avec aujourd'hui puisque sans cesse, en art, en sciences, dans la publicité, il est fait référence à l'antiquité.

Il n'est pas nécessaire d'avoir fait du latin et/ou du grec au collège, cette option est ouverte aux grands débutants... il faut seulement mentionner sur la fiche d'inscription au lycée que l'on veut faire latin-grec pour être dans une classe dont l'emploi du temps est compatible avec l'option.

## Le programme de seconde

Un programme fondé sur la confrontation entre mondes anciens et monde moderne : c'est d'abord un questionnement sur l'Homme lui-même qui est proposé aux élèves : qu'est-ce qui fait le propre de l'Homme ? Comment devenir pleinement humain ?

### Littérature, civilisation, culture, histoire

La lecture des œuvres et des textes majeurs de la littérature gréco-latine, situés dans leur contexte, constitue le socle de l'apprentissage. Les œuvres et les textes sont abordés selon diverses modalités de lecture : en traduction, en lecture bilingue, en langue originale ; dans leur intégralité ou en extraits. Ils sont confrontés à des œuvres modernes et contemporaines, issues de la littérature française ou étrangère, avec lesquelles ils entrent en résonance.

### L'homme et l'animal

L'Homme, un animal comme les autres ?

- Mutations, transformations, monstres et hybrides.
- Regards de l'Homme sur l'animal : animaux réels, animaux fabuleux ; encyclopédies et bestiaires.
- Des animaux et des hommes : amis ou ennemis ?

### L'Homme et le divin

- Hommes, héros et dieux
- Métamorphoses : quand l'homme devient dieu, quand le dieu devient homme.
- Le voyage aux Enfers.

### Soi-même et l'autre

Différence de culture et de condition : Grecs, Romains et barbares : hommes libres et esclaves.

- La langue de l'autre : échanger et dialoguer.
- Un autre monde : apparitions, fantômes et spectres.
- L'autre en soi : dédoublement, possession et aliénation.

### Méditerranée : voyager, explorer, découvrir

« Notre mer » : une mosaïque de peuples, un espace polycentré.

- Aux confins du monde habité : terres connues et inconnues.
- Voyages et périples héroïques.
- Accueil et hospitalité : étrangers et exilés.

## La place du numérique

Pour aborder tous ces thèmes, nous utilisons souvent le recours à l'image et aux réalisations numériques contemporaines qui permettent de rendre le cours vivant et attractif.

---

!!! info "Deux documentaires hébergés sur YouTube.com"
    En cliquant sur les liens ci-dessous, vous acceptez les [conditions générales](https://www.youtube.com/static?template=terms&gl=FR) de YouTube.com

    - [Des Racines et des Ailes : le Parthénon retrouve ses couleurs](https://www.yout-ube.com/watch?v=7GGa4ZEvtA0)

    - [Arte - Brève histoire de la mythologie grecque](https://www.yout-ube.com/watch?v=qNxpOfGTDfg)

    <p><a href="https://commons.wikimedia.org/wiki/File:The_Parthenon_in_Athens.jpg#/media/Fichier:The_Parthenon_in_Athens.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/d/da/The_Parthenon_in_Athens.jpg" alt="Image dans Infobox."></a><br><a href="https://creativecommons.org/licenses/by/2.0" title="Creative Commons Attribution 2.0">CC BY 2.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=17065839">Lien</a></p>

### Odysseum

[ODYSSEUM-édu**scol**](https://eduscol.education.fr/odysseum/lycee), Maison numérique des Humanités, entend renforcer les synergies entre un enseignement en présence des élèves, toujours aussi crucial, et un accompagnement à distance, de manière à être au plus près des professeurs dans leurs classes. Il s'agit de leur apporter les ressources et les outils usuels, mais aussi variés et innovants pour la construction de leur cours, afin de favoriser les parcours autonomes et personnalisés de leurs élèves.


--8<-- "includes/abbreviations.md"
