# Bienvenue au lycée Lucie AUBRAC

!!! info inline end "Le logo du lyceé"
    ![Logo du lycée](assets/site.png)
    
    Le logo du lycée est une création de Ayse Tiram, élève de première spécialité Arts Plastiques suite à un concours où plus de 10 projets ont été soumis au vote de l'ensemble de communauté de l'établissement scolaire.

Une présentation du lycée qui aidera à remplacer les Journées Portes Ouvertes (JPO).

De manière virtuelle, vous pouvez découvrir les locaux, les enseignements et obtenir quelques renseignements utiles sur l'ENT ou l'intendance de l'établissement.

![entrée du lycée](img/entrée-Lycée.jpg)

## Administration

Proviseur : M. VINATIER

Proviseur adjoint : Mme RODRIGUES

Gestionnaire : M. MOREL

CPE : M. PÉAN

![cour du lycée](img/cours_lycée.jpg)

## Les enseignements

Retrouvez [la liste complète en cliquant ici.](ens/enseignements.md)

### Enseignements de spécialité

En plus du tronc commun, le lycée offre des spécialités parmi ses enseignements :

- [Arts Plastiques](ens/arts-pla.md)
- [Histoire Géographique Géopolitique et Sciences Politiques](ens/hist.md)
- [Humanité, Littérature et Philosophie](ens/philo.md)
- [Langues, Littérature et Civilisation Étrangères](ens/anglais/#presentation-de-la-specialite-llce)
- [Mathématiques](ens/maths.md#en-classe-de-premiere)
- [Numérique et Sciences Informatiques](ens/info.md#la-specialite-nsi)
- [Physique - Chimie](ens/physique.md#en-classe-de-premiere-et-terminale-generales)
- [Sciences Économiques et Sociales](ens/ses.md)
- [Sciences de la Vie et de la Terre](ens/svt.md)

### Options facultatives du lycée

- En seconde
    - [Arts plastiques](ens/arts-pla.md)
- De la seconde à la terminale
    - [Éducation Physique et Sportive](ens/eps.md#iii-les-activites-proposees-en-enseignement-optionnel)
    - [Latin](ens/latin-grec.md)
    - [Section Européenne anglais](ens/section_euro_en.md)
    - [Section Européenne italien](ens/section_euro_it.md)
- En terminale uniquement :
    - [Droit et Grand Enjeu du Monde Contemporain](ens/DGEMC.md)
    - [Mathématiques Complémentaires](ens/maths.md#en-classe-de-terminale)
    - [Mathématiques Expertes](ens/maths.md#en-classe-de-terminale)

--8<-- "includes/abbreviations.md"
