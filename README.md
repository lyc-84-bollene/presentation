# Dépôt du code source

Pour la présentation du lycée, un site web est créé.

Dans le répertoire `docs`, on trouve les fichiers utiles au site web.

- puis dans le répertoire `ens` chaque discipline y est représenté.

## Comment participer ?

1. Identifier le fichier qui est utile.
2. Téléchargez-le (à droite, `Download`).
3. Modifiez-le avec votre éditeur _Bloc Note_,
    - ou bien avec **CodiMD** que vous trouverez dans https://apps.education.fr/
4. Envoyez-moi par mail la version modifiée.

