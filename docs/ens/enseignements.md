# Les enseignements au lycée

- Les langues : Allemand, Anglais, Espagnol, Italien

- Arts plastiques
- Centre de Documentation et d'Information
- Éducation Physique et Sportive
- Français
- Philosophie
- Histoire Géographie Éducation civique
- Informatique
- Mathématiques
- Sciences Économiques et Sociales
- Sciences physiques et Chimie
- Sciences et Technologies du Management et de la Gestion
- Sciences de la Vie et de la Terre
