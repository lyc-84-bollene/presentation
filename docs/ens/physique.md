# Sciences Physiques

Pour l'enseignement des sciences physiques, le lycée Lucie AUBRAC
dispose de quatre salles de travaux pratiques : deux salles de chimie
(A107 et A109) et deux salles de physique (A111 et A113) et de deux
salles de cours (A106 et A114).

Les salles de TP peuvent contenir de 18 à 24 élèves et sont
constituées de 9 à 12 postes de travail équipés en ordinateur pour
accueillir un binôme d'élèves.

## En classe de seconde

L'élève aura 3 h de sciences physiques réparties actuellement en 2 h de
cours (classe entière) et 1 h en effectif réduit (classe dédoublée pour
les travaux pratiques)

- En prolongeant les thèmes du collège, cet enseignement propose
 d'**explorer le réel, de l'infiniment petit à l'infiniment grand**.
  Le programme de seconde est structuré autour de quatre
 thèmes qui portent sur la constitution et les transformations de
 la matière ; le mouvement et les interactions ; les ondes et les
 signaux ; les conversions et transferts d'énergie. Ces thèmes
 permettent de traiter de nombreuses situations de la vie
 quotidienne.

- Cet enseignement vise à favoriser la pratique expérimentale et
 l'activité de modélisation. L'objectif est de donner aux
 élèves une vision intéressante et authentique de la
 physique-chimie.

- [Consultez le programme complet de physique-chimie en 2de](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/98/9/spe634_annexe_1062989.pdf)

## En classe de Première et Terminale générales

L'enseignement des sciences physiques dans l'enseignement commun
n'apparaît plus en tant que matière, il s'intègre dans
 un **enseignement scientifique** à raison de 2 h hebdomadaire où la SVT et les sciences
physiques interviennent.

- L'enseignement scientifique en classe de première générale a pour
 but d'aider les élèves à cerner ce que la connaissance
 scientifique a de spécifique dans ses pratiques, dans ses méthodes
 d'élaboration et dans ses enjeux de société. Il permet aussi de
 consolider la culture et le raisonnement scientifique des élèves.

- [Consultez le programme complet de l'enseignement scientifique en 1re générale](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/13/4/spe573_annexe_1063134.pdf)

La **physique-chimie** est une des **spécialités** proposées au lycée.
L'élève suit **4 h en première** de cet enseignement par semaine comme les deux
autres spécialités choisies et l'horaire hebdomadaire
passe à **6 h en Terminale.** (L'élève conserve deux spécialités parmi
les trois suivies en première). Si le nombre d'élèves présents dans un
groupe est élevé, le groupe est dédoublé sur une séance de 2 h pour
pouvoir effectuer les travaux pratiques.

- L'enseignement de spécialité « Physique-chimie » s'appuie sur
 la pratique expérimentale et l'activité de modélisation pour
 permettre aux élèves d'établir un lien entre le monde des objets,
 des expériences, des faits et celui des modèles et des théories.

- Le programme est structuré autour de **quatre thèmes** qui
 permettent de prendre appui sur de nombreuses situations de la vie
 quotidienne et de contribuer à faire du lien avec les autres
 disciplines scientifiques :
    - Constitution et transformations de la matière,
    - Mouvement et interactions
    - L'énergie : conversions et transferts
    - Ondes et signaux

-   Les nombreux domaines d'applications donnent à l'élève une image
    concrète, vivante et actuelle de la physique et de la chimie.

-   [Consultez le programme complet de physique-chimie](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/43/2/spe635_annexe_1063432.pdf)

--8<-- "includes/abbreviations.md"
