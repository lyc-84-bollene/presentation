# DGEMC (Droit et Grands Enjeux du Monde Contemporain)

L'enseignement de droit et grands enjeux du monde contemporain (DGEMC) est un **enseignement optionnel** de 3 heures proposé aux élèves de **terminale générale**, quels que soient les enseignements de spécialité suivis.

Cet enseignement vise à élargir les perspectives des élèves en leur faisant découvrir les instruments du droit, son rôle social et la méthodologie du raisonnement juridique. Les élèves seront ainsi amenés à aborder certains grands thèmes du monde contemporain à travers la manière dont ils sont saisis par le droit.

Cet enseignement ne constitue pas un préalable pour une poursuite d'études juridiques, mais permet aux élèves de saisir le rôle du droit dans de nombreux domaines et contribue ainsi à leur formation de citoyens éclairés.

## Logique du programme

L'introduction et la première partie du programme vise à doter les élèves des **notions juridiques élémentaires** (fonctions du droit, caractéristiques de la règle de droit, sources du droit, institutions, organisation judiciaire française et internationale) pour leur permettre de comprendre et mener un raisonnement juridique.

La seconde partie du programme vise le traitement, sous l'angle juridique, de plusieurs **grandes questions contemporaines** dans des domaines très variés qui peuvent correspondre à des projets de poursuite d'études divers.

### Exemples de questions

- L'animal est-il une personne ou une chose ?
- Dans quelle mesure l'État peut-il limiter la liberté des individus ?
- Pourquoi protéger les droits de l'enfant ?
- Pourquoi est-il utile de réfléchir sur les pratiques médicales sur le vivant ?
- Que recouvre le principe de liberté sexuelle ?
- À quelles conditions juridiques une entreprise peut-elle être responsable d'un préjudice écologique ?
- Qui protège les données à caractère personnel ?
- Quels défis l'intelligence artificielle pose-t-elle pour les juristes ?
- ...

--8<-- "includes/abbreviations.md"
