# Informatique

## En classe de seconde

L'enseignement de la SNT est de 1 h 30 par semaine.

M. Chambon vous propose un parcours de [découverte de Python](https://ens-fr.gitlab.io/algo0/
)

## La spécialité NSI

**NSI** : **N**umérique et **S**ciences **I**nformatiques

En classe de première et terminale, on apprend à coder, surtout en **Python**. On réfléchit à des **algorithmes**, et on résout des problèmes.

### Exemples

- Voici un exemple d'exercice résolu niveau Première :

> Clique sur `Exécuter`, puis entre ton prénom.

<iframe src="https://brython.info/tests/editor.html?lang=fr&code=%22%22%22%0AUn%20petit%20jeu%20d'affichage%0A%22%22%22%0A%0Apr%C3%A9nom%20%3D%20input%28%22Quel%20est%20ton%20pr%C3%A9nom%20%3F%22%29%0A%0Al%20%3D%20len%28pr%C3%A9nom%29%0An%20%3D%202%20*%20l%20-%201%0A%0Adef%20num%C3%A9ro%28i%2C%20j%29%3A%0A%20%20%20%20i%20%3D%20min%28i%2C%20n%20-%201%20-%20i%29%0A%20%20%20%20j%20%3D%20min%28j%2C%20n%20-%201%20-%20j%29%0A%20%20%20%20return%20min%28i%2C%20j%29%0A%0A%0Afor%20i%20in%20range%28n%29%3A%0A%20%20%20%20for%20j%20in%20range%28n%29%3A%0A%20%20%20%20%20%20%20%20print%28pr%C3%A9nom%5Bnum%C3%A9ro%28i%2C%20j%29%5D%2C%20end%3D%22%22%29%0A%20%20%20%20print%28%29%0A" width="100%" height="500"></iframe>

- Voici un autre exemple niveau début Terminale.

> Ce script récupère, en le vérifiant, un entier de 1 à 1000, puis affiche ses diviseurs, ainsi que la somme.

<iframe src="https://console.basthon.fr/?script=eJx1UktqwzAU3Bt8h1dBwSYhibehLhS67KoXcD56bgTNk6tPSDE5R7e9Sy7Wp9hOlbTRwpinmdGMRhJrkJoIK08V6e3KYJbP0wR4Odw7hBIUNd5l4jnAwBN0MJAIBRy_oZjNZjAXeU8ynz09LDrRXXaSyn_nS2vROOY_lIzhTxDptnG_xsZFGgadN_TXZYfodylN0kRyGKu3W6yk2mU05BBCvCLttEJ4X3YANm-BQcqiNzZEWdBi0sGf6lqtN4xlSGO09Mrxj7ZWrXg0CXId8HxUr1JCH6HWBiTnBrOkN8yKMWccQZFHoT68dgrJQcg_nYKM89rTtRPcx2PFRcHjmRhpRddwZemC3euyyytuY0JFtWjlAY5f0A5HHNhESweRX8KvY4_K2ObZ6l15y-sNkQEdmgzv5p_C02Tw-nKzSHYMnFSML54Ck38ACQvRtQ" width="100%" height="900"></iframe>

### Une nouvelle discipline

- On part de zéro ; aucune connaissance n'est requise.

- On prépare surtout les élèves scientifiques à pouvoir aborder les études pour être ingénieur. Mathématiques, **Informatique** et Physique y sont les matières principales.

- Mais pas seulement...

### Le volume horaire

- En première, par semaine :
    - 2 h de cours pour apprendre,
    - 2 h de TD pour s'entraîner.

- En terminale, par semaine :
    - 4 h de cours pour apprendre,
    - 2 h de TD pour s'entraîner.

### Le cours

- Le cours est disponible en ligne, on n'a pas besoin de l'écrire. Le professeur l'explique en détail au tableau ou en visio.

- Il est conseillé de le relire plusieurs fois, on y apprend toujours un détail de plus.

En voici une partie :

- https://ens-fr.gitlab.io/algo1/
- https://ens-fr.gitlab.io/algo2/


### Les TD

- Chaque élève peut avancer à son rythme.
- Certains élèves sont très rapides, ils peuvent prendre de l'avance sans s'ennuyer.
- Certains élèves ont besoin de temps au démarrage, et ne veulent pas être stressés, ils peuvent avancer plus tranquillement.

### Les épreuves du BAC

- Si on arrête la spécialité NSI en fin de première, l'épreuve est uniquement un QCM. Les élèves auront appris des bases très utiles pour toute carrière professionnelle à venir.
- Si on garde la NSI en terminale, il y a une épreuve écrite sur 12, et une épreuve pratique sur 8. Il y a plus de difficultés théoriques ; on travaille sur des structures de données abstraites, comme des piles, des files, des arbres, des graphes. Entre autres choses.

### Les contrôles

- En première, les interrogations sont 3/4 QCM, 1/4 écrit.
- En terminale, ce sont des épreuves à l'écrit, ou sur machine.
- Il y a aussi des devoirs maison notés de programmation en Python.

### Pour quels élèves ?

1. Pour ceux qui veulent faire ingénieur, il **faut** prendre Mathématiques, **NSI** et Physique en première. En terminale, il faut garder les Mathématiques et choisir l'une des deux autres suivant ses goûts.

2. La **NSI** se couple aussi très bien avec la spécialité **Arts plastiques** pour pouvoir faire architecte.

3. La **NSI** se couple aussi très bien avec la spécialité **SVT**, où très clairement l'avenir est très prometteur en Bio-Informatique.

4. L'immense majorité des nouveaux métiers qui arrivent sont liés à ... l'**Informatique**.

### Et les filles ?

Il y a parfois un stéréotype qui induit que la NSI c'est plus pour les garçons ; c'est **FAUX**.

- Dans de nombreux pays, c'est le contraire.
- Le travail avec l'informatique n'est pas rude physiquement, permet le télé-travail.
- Les filles réussissent très bien.

Plus d'information : https://www.ac-paris.fr/portail/jcms/p2_2508811/les-rencontres-du-numerique-pour-lyceennes-et-lyceens-de-nsi

### Métiers recherchés

Les métiers rémunérateurs (pour gagner de l'argent) les plus recherchés sont très souvent dans l'**Informatique**.

> Cherchez et vérifiez !!!

- [Un article du 12/01/2022](https://www.zdnet.fr/actualites/trouver-des-developpeurs-va-etre-votre-plus-gros-casse-tete-cette-annee-39935405.htm)
- [La discussion de professionnels associée du 14/01/2022](https://linuxfr.org/users/ysabeau/liens/trouver-des-developpeurs-va-etre-votre-plus-gros-casse-tete-cette-annee-python-java-javascript)


### Beaucoup d'avantages

Travailler dans l'informatique, c'est pouvoir très facilement trouver un travail :

- Dans la ville où l'on souhaite aller, voire télétravailler à 100% d'où on veut !
- Avec un bon salaire.
- Avec des collègues plutôt jeunes.
- Avec des choses nouvelles à apprendre régulièrement ; on ne s'ennuie pas.

### C'est difficile ?

Oui, parfois. Le contenu du cours est assez imposant et tout comprendre est réservé aux meilleurs élèves qui seront ingénieurs.

Mais pas toujours et les épreuves du BAC sont très accessibles. Par exemple, **savoir coder un calcul de moyenne** permet souvent d'avoir la moyenne à l'épreuve pratique.

### À quoi ça ressemble ?

- On ne va pas vous mentir, on ne va pas coder des jeux vidéos en 3D. Mais un jour, vous pourrez !

- On passe beaucoup de temps à résoudre des petits problèmes, avec très peu de maths, mais avec beaucoup de logique.

- On ne fera pas du tout de bureautique ; ce n'est pas le programme. Pour de la bureautique, il vaut mieux aller en STMG.

- Le programme est axé autour du code (programmation) et architecture (matériel et réseau).

### Avec quel matériel ?

Les élèves utilisent peu la tablette, mais beaucoup l'ordinateur. On n'utilise que des **logiciels libres** et multiplateformes, donc ils sont gratuits et utilisables avec Windows, Mac ou Linux ; au choix à la maison.

### Avec quels logiciels ?

- Python, comme langage principal.
- Thonny et VSCodium comme éditeurs.
- VirtualBox pour faire de la virtualisation système.
- Termux et micro pour jouer sur la tablette.
- On fait aussi de la simulation de réseau avec Filius.
- On peut apprendre à créer un site web avec GitLab et MkDocs.

### Un témoignage externe

En cliquant sur ce lien vidéo, vous acceptez les [conditions générales](https://www.youtube.com/static?template=terms&gl=FR) de YouTube.com

<iframe width="560" height="315" src="https://www.youtube.com/embed/e27wIpjsV_E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


--8<-- "includes/abbreviations.md"
