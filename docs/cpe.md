# Pôle Vie Scolaire

Le pôle Vie Scolaire est composé d'un Conseiller Principal d'Éducation et de 6 Assistants d'Éducation. 

Le **Bureau de la Vie Scolaire** est ouvert du lundi au vendredi
 de 7 h 45 à 12 h et de 13 h à 18 h.
 Il est fermé le mercredi après-midi.

Les élèves y sont accueillis pour régulariser leurs absences, et pour toute demande de renseignements. Le pôle Vie Scolaire se charge de mettre en relation les usagers avec les autres services du lycée. Il propose une écoute aux élèves qui en éprouvent le besoin. Ses personnels, notamment les **Assistants d'Éducation**, sont chargés de la surveillance des lycéens et de l'application du règlement intérieur dans l'enceinte du lycée.

Le **Conseiller Principal d'Éducation** est présent tous les jours :

| Jour | Horaire |
|------|---------|
| Lundi    | de 13 h à 18 h 30   |
| Mardi    | de 7 h 45 à 18 h 20 |
| Mercredi | de 7 h 30 à 12 h 20 |
| Jeudi    | de 13 h à 18 h 30   |
| Vendredi | de 7 h 45 à 18 h 20 |

Il reçoit les élèves et les parents _de préférence sur rendez-vous_.
