# SVT

Les SVT correspondent aux sciences de l'étude du vivant, de la Terre et de l'environnement. C'est une discipline qui mêle théorie et pratique au travers de démarches scientifiques qui rendent les élèves acteurs de leur apprentissage.

## Volume horaire

Au lycée, en classe de seconde, les élèves ont 1 h 30 de SVT par semaine :

- 1 h de cours par quinzaine.
- 1 h de Travaux Pratiques en effectif réduit par semaine.

## Le matériel

Les travaux pratiques se déroulent dans des salles spécialisées, richement équipées :

- Ordinateurs avec de nombreux logiciels.
- Vidéo-microscopes, microscopes polarisants, loupes binoculaires.
- Matériel ExAO (Expérimentation Assistée par Ordinateur).

## Objectifs

Les SVT au lycée poursuivent 3 objectifs majeurs :

- Maîtriser les connaissances scientifiques et le mode de raisonnement propre aux sciences.
- Participer à la formation de l'esprit critique.
- Préparer les élèves qui choisiront de poursuivre leurs études dans une formation scientifique : biologie, géologie, environnement ou santé.

## Thématiques

Pour atteindre ces objectifs, le programme de la classe de seconde est organisé en trois grandes thématiques :

- La Terre, la vie et l'évolution du vivant.
- Enjeux contemporains de la planète.
- Corps humain et santé.


## Quelques photos en classe

> Crédit : M. Bacon

![](svt/1.JPG)

![](svt/2.JPG)

![](svt/3.JPG)

![](svt/4.JPG)

![](svt/5.JPG)


--8<-- "includes/abbreviations.md"
