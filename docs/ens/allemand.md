# Allemand

Il s'agit d'un enseignement dans la continuité du collège ciblant les compétences comme la compréhension de l’oral et de l’écrit et l'expression écrite et orale.

## Volume horaire

Les horaires en seconde sont de 2 h 30 par semaine.

## Programme

Les thèmes abordés sont les mêmes dans toutes les langues et suivent les programmes officiels (p. ex : famille, sport, art, ville...)

Les documents étudiés sont proposés sur des supports variés (p. ex. articles de journaux, chansons, films, reportages audio, extraits de textes littéraires, photos et autres supports iconographiques...)
Nous veillons à mettre tous les élèves à niveau en début de Seconde afin de leur permettre d'aborder les épreuves du bac qui débutent au milieu de la Première avec plus de sérénité.

Par ailleurs, les élèves volontaires ont la possibilité de participer aux projets divers comme les échanges, les voyages, les sorties, etc.

## Quelques photos

### Le château de Neuschwanstein

<p><a href="https://commons.wikimedia.org/wiki/File:Schloss_Neuschwanstein_2013.jpg#/media/Fichier:Schloss_Neuschwanstein_2013.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Schloss_Neuschwanstein_2013.jpg/1200px-Schloss_Neuschwanstein_2013.jpg" alt="Image illustrative de l’article Château de Neuschwanstein"></a><br><a href="https://creativecommons.org/licenses/by-sa/3.0/de/deed.en" title="Creative Commons Attribution-Share Alike 3.0 de">CC BY-SA 3.0 de</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=27899216">Lien</a></p>

### La porte de Brandebourg

<p><a href="https://commons.wikimedia.org/wiki/File:Berlin_-_0266_-_16052015_-_Brandenburger_Tor.jpg#/media/Fichier:Berlin_-_0266_-_16052015_-_Brandenburger_Tor.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Berlin_-_0266_-_16052015_-_Brandenburger_Tor.jpg/1200px-Berlin_-_0266_-_16052015_-_Brandenburger_Tor.jpg" alt="Image dans Infobox."></a><br><a href="https://creativecommons.org/licenses/by/4.0" title="Creative Commons Attribution 4.0">CC BY 4.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=40351258">Lien</a></p>


--8<-- "includes/abbreviations.md"
