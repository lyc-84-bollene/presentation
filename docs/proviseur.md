# Le mot du proviseur

## présentation de l'établissement

### Le cadre

Le lycée Lucie Aubrac de Bollène a ouvert ses portes en septembre 2006. À la rentrée scolaire 2021, l'établissement entrera dans sa seizième année de fonctionnement et accueillera 640 élèves. La structure se compose cette année de 9 divisions de seconde, 5 divisions de première générale, 3 divisions de première STMG, 5 divisons de terminale générale et trois divisions de terminale STMG.

Le lycée Lucie Aubrac est un établissement agréable. De larges baies vitrées, des espaces verts intérieurs, une cour pavée, des palissades en bois procurent un caractère apaisant à l'ensemble. Il dispose d'équipements récents.

### Les infrastructures sportives

Les activités sportives se déroulent dans le gymnase de l'établissement et sur le plateau sportif. Le lycée utilise également le stade municipal Anquetil de Bollène. Il est doté de deux salles informatiques pour les élèves du lycée général et de trois salles équipées d'ordinateurs pour les séries STMG. L'établissement a fait le choix, il y a maintenant trois ans, de travailler avec des tablettes numériques. En langue, l'offre de formation est importante : anglais, allemand, espagnol et italien. Le lycée accueille 66 élèves de section européenne en anglais.

### Les résultats aux examens

Concernant les résultats aux examens, les taux de réussite au baccalauréat en série générale se situent au niveau des résultats nationaux et le taux de réussite en filière STMG est lui supérieur de 7 points avec un taux de mention également supérieur de 10 points aux chiffres du département. Le lycée Lucie Aubrac occupe en 2020 la 6^e^ place au classement des lycées du Vaucluse.

### Les enseignements

Le corps professoral se compose de 56 enseignants. Ils sont très investis dans la réussite de leurs élèves et ils construisent de nombreux outils pédagogiques en commun. Attentifs aux besoins des élèves, les professeurs proposent des projets ambitieux : éducation aux médias, atelier théâtre et prix Godot, projet mémoire et citoyenneté, projets artistiques, projet Écologie et Architecture, lycéens au cinéma, concours de l'éloquence. Dans une optique de décloisonnement, ces projets sont menés en interdisciplinarité et parfois en mélangeant les filières (générale et STMG). L'ouverture à l'international dynamise le lycée : une section euro, Erasmus+.

### Le projet d'établissement

Le projet d'établissement comporte de nombreuses actions qui s'appuient sur l'axe « ouverture culturelle ». Une forte dynamique anime l'établissement autour de certains pôles : les enseignements artistiques, l'EPS et le CDI avec la mise en place de l'accès à [Pix](https://pix.fr/). Les équipes portent de nombreux projets dont des sorties culturelles et des voyages scolaires. Par ailleurs, le lycée est engagé dans le projet [Eramus+](https://info.erasmusplus.fr/).

### Le Conseil de Vie Lycéenne (CVL)

Les réunions du CVL sont riches et porteuses de projets, des professeurs y participent. Les activités de la MDL sont nombreuses et contribuent à insuffler un esprit « Lycée Aubrac » aux élèves. Le dynamisme de l'association sportive (22.48% des élèves) et l'implication forte des professeurs d'EPS favorisent ce bon état d'esprit. En interne, les lycéens ont souhaité développer le vivre ensemble, l'esprit de solidarité et d'entraide en créant des manifestations sportives ou culturelles mixtes sur tous les niveaux et sur toutes les filières. Certains temps forts, regroupant des élèves et des adultes fédèrent l'établissement : fête de Noël, carnaval, printemps des lycéens, bal des terminales. Certaines de ces actions ont été suspendues cette année en raison de la pandémie. Un CESC actif mène des actions sur la prévention des conduites addictives, la sécurité routière, la gestion du stress, l'accès au droit, l'éducation au numérique, l'alimentation et le secourisme.

### Le service de restauration

Le lycée est pourvu d'un service de restauration assurant environ 550 repas par jour. Cette année, en raison des contraintes sanitaires, l'organisation du passage des élèves a été repensée. Ainsi, 4 services ont été organisés entre 11 h 20 et 13 h 30 depuis le 1^er^ septembre, chacun pouvant accueillir 150 élèves à la demi-pension. Un système à carte remplace depuis la rentrée scolaire 2020 le système de reconnaissance de l'empreinte de la main qui n'est pas compatible avec les règles sanitaires. Enfin, la qualité des repas est reconnue par l'ensemble des usagers.

### L'équipe de direction

L'équipe de direction est quant à elle composée d'un Proviseur, d'une Proviseure Adjointe et d'un agent comptable gestionnaire. Ces personnels travaillent avec des collaborateurs qui sont au nombre de 4 : une fondée de pouvoir à l'intendance, une secrétaire du proviseur, une secrétaire élève ainsi qu'une secrétaire d'intendance.

--8<-- "includes/abbreviations.md"
