# La section européenne SVT en italien

**SVT** : **S**ciences de la **V**ie et de la **T**erre

**DNL** : **D**iscipline **N**on **L**inguistique

UN PERCORSO PER STUDIARE BIOLOGIA IN UN MODO DIVERSO ED
APRIRSI ALL'EUROPA.

## Les objectifs

- Améliorer la maitrise de la langue italienne.
- Enrichir la culture générale.
- Développer la réflexion et l'ouverture d'esprit.
- Travailler autrement en italien par le biais de travaux pratiques, de l'utilisation de la méthode scientifique...
- Travailler autrement en langue étrangère par le biais d'échanges linguistiques de sorties ou voyages scolaires.

## Les moyens

- Une heure de renforcement linguistique en italien.
- Un enseignement de Sciences de la Vie et de la Terre, discipline dite non linguistique (DNL) dispensé en italien.
- Échanges linguistiques et culturel.

## Les enjeux

Obtenir la mention européenne sur le diplôme du baccalauréat.

- Constitue une valeur ajoutée sur les dossiers de candidature pour certaines écoles, classes préparatoires et universités.
- Donne des bases solides pour des études supérieures à vocation internationale (le droit, le commerce, le tourisme, le journalisme...)
- Permet aux futurs étudiants de partir plus facilement étudier à l'étranger dans le cadre d'échanges universitaires comme Erasmus.

## Le public

- Des élèves motivés par la langue et la culture italiennes, avec un niveau suffisant pour s'exprimer dans cette langue à l'oral et à l'écrit.
- Des élèves ayant rempli un dossier de candidature, dans leur collège, avant la commission de sélection des dossiers qui se déroule au mois de juin.

