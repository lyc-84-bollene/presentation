# Philosophie

En terminale, tous les élèves suivent en tronc commun un enseignement de philosophie, où ils ont l’occasion d’étudier les plus belles barbes de l’histoire, de [Platon](https://upload.wikimedia.org/wikipedia/commons/d/da/Plato_Pio-Clemetino_Inv305.jpg) à [Bachelard](https://upload.wikimedia.org/wikipedia/commons/c/cb/Gaston_Bachelard_1965.jpg) en passant par [Marx](https://upload.wikimedia.org/wikipedia/commons/8/87/Karl_Marx.png), ainsi que la moustache à la gauloise de [Nietzsche](https://upload.wikimedia.org/wikipedia/commons/1/1b/Nietzsche187a.jpg) et la coupe mulet de [Descartes](https://upload.wikimedia.org/wikipedia/commons/7/73/Frans_Hals_-_Portret_van_Ren%C3%A9_Descartes.jpg). 

Mais il ne s’agit pas seulement d’**acquérir une culture philosophique initiale** : l’objectif de cet enseignement est d’aider les élèves à devenir des citoyens capables de **développer une réflexion éclairée et autonome**, en les y exerçant.

Le cours de philosophie consiste donc à explorer diverses questions portant sur les grands aspects de l’existence humaine (la liberté, l’art, la science, la politique, la morale, etc.) en mobilisant *toute* la culture des élèves (vie quotidienne, connaissances personnelles, autres disciplines, etc.).

## Programme

Le programme consiste en une liste de **notions** (art, bonheur, justice, liberté, travail, vérité, etc.) et une liste d’**auteurs** ([Aristote](https://upload.wikimedia.org/wikipedia/commons/a/ae/Aristotle_Altemps_Inv8575.jpg), Descartes, [Kant](https://upload.wikimedia.org/wikipedia/commons/4/43/Immanuel_Kant_%28painted_portrait%29.jpg), etc.), chaque notion étant librement traitée par l'enseignant à travers les questions de son choix, en s’appuyant sur différents auteurs, des événements historiques, des œuvres d'art, la culture des élèves, etc.

## Exercices et épreuve du baccalauréat

Toute l'année, les élèves s'exerceront à l'épreuve du baccalauréat, qui leur demandera de traiter un sujet au choix parmi trois, toujours répartis en deux sujets de dissertation et un sujet d'explication de thèse.

- **Dissertation** : il s’agit d’exercer les élèves à la réflexion en leur demandant de construire eux-mêmes une réflexion qui traite méthodiquement et progressivement une question donnée, à partir d’exemples, de connaissances et de concepts.

- **Explication de texte** : il s’agit de former les élèves à la réflexion en les confrontant à des textes qui leur donnent des exemples de réflexion, pour y identifier la question et le problème, l’argumentation, les concepts, les exemples, etc.

## Volume horaire

|Terminale technologique|Terminale générale|
|:---:|:---:|
|Deux heures par semaine | Quatre heures par semaine|


## Galerie de portraits

### Platon

<p><a href="https://commons.wikimedia.org/wiki/File:Plato_Pio-Clemetino_Inv305.jpg#/media/Fichier:Plato_Pio-Clemetino_Inv305.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Plato_Pio-Clemetino_Inv305.jpg/1200px-Plato_Pio-Clemetino_Inv305.jpg" alt="Image dans Infobox."></a><br>Domaine public, <a href="https://commons.wikimedia.org/w/index.php?curid=1307144">Lien</a></p>

### Gaston Bachelard

<p><a href="https://commons.wikimedia.org/wiki/File:Gaston_Bachelard_1965.jpg#/media/Fichier:Gaston_Bachelard_1965.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Gaston_Bachelard_1965.jpg/1200px-Gaston_Bachelard_1965.jpg" alt="Image dans Infobox."></a><br><a href="https://creativecommons.org/licenses/by-sa/3.0/nl/deed.en" title="Creative Commons Attribution-Share Alike 3.0 nl">CC BY-SA 3.0 nl</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=37090666">Lien</a></p>

### Karl Marx

<p><a href="https://commons.wikimedia.org/wiki/File:Karl_Marx.png#/media/File:Karl_Marx.png"><img src="https://upload.wikimedia.org/wikipedia/commons/8/87/Karl_Marx.png" alt="Karl Marx.png"></a><br> Domaine public, <a href="https://commons.wikimedia.org/w/index.php?curid=6875847">Lien</a></p>

### Friedrich Nietzsche

<p><a href="https://commons.wikimedia.org/wiki/File:Nietzsche187a.jpg#/media/Datei:Nietzsche187a.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Nietzsche187a.jpg/1200px-Nietzsche187a.jpg" alt="Nietzsche187a.jpg"></a><br>Domaine public, <a href="https://commons.wikimedia.org/w/index.php?curid=95970">Lien</a></p>

### René Descartes

<p><a href="https://commons.wikimedia.org/wiki/File:Frans_Hals_-_Portret_van_Ren%C3%A9_Descartes.jpg#/media/Fichier:Frans_Hals_-_Portret_van_René_Descartes.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/7/73/Frans_Hals_-_Portret_van_Ren%C3%A9_Descartes.jpg" alt="Image dans Infobox."></a><br>Domaine public, <a href="https://commons.wikimedia.org/w/index.php?curid=2774313">Lien</a></p>


### Aristote

<p><a href="https://commons.wikimedia.org/wiki/File:Aristotle_Altemps_Inv8575.jpg#/media/Datei:Aristotle_Altemps_Inv8575.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Aristotle_Altemps_Inv8575.jpg/1200px-Aristotle_Altemps_Inv8575.jpg" alt="Aristotle Altemps Inv8575.jpg"></a><br>Domaine public, <a href="https://commons.wikimedia.org/w/index.php?curid=1359807">Lien</a></p>

### Kant

<p><a href="https://commons.wikimedia.org/wiki/File:Immanuel_Kant_(painted_portrait).jpg#/media/Datei:Immanuel_Kant_(painted_portrait).jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/4/43/Immanuel_Kant_%28painted_portrait%29.jpg" alt="Immanuel Kant (painted portrait).jpg"></a><br>Domaine public, <a href="https://commons.wikimedia.org/w/index.php?curid=897016">Lien</a></p>


--8<-- "includes/abbreviations.md"
