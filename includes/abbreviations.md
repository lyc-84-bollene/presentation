*[BAC]: Baccalauréat
*[ENT]: Environnement Numérique de Travail
*[NSI]: Numérique et Sciences Informatiques
*[SNT]: Sciences Numériques et Technologie
*[SVT]: Sciences de la Vie et de la Terre
*[CDI]: Centre de Documentation et d'Information
*[EPS]: Éducation Physique et Sportive
*[STMG]: Sciences et Technologies du Management et de la Gestion
*[HG-EC]: Histoire - Géographie - Éducation Civique
*[SES]: Sciences Économiques et Sociales
*[QCM]: Questionnaire à Choix Multiple
*[SNT]: Sciences Numériques et Technologie
*[JPO]: Journées Portes Ouvertes
*[DNL]: Discipline Non Linguistique
*[TD]: Travaux Dirigés
*[LVA]: Langue Vivante - A
*[CEC]: _Cambridge English Certificate_
*[LLCE]: Langues, Littératures et Civilisations Étrangères
*[ECLA]: Enseignement Conjoint Langues Anciennes
*[DGEMC]: Droit et Grands Enjeux du Monde Contemporain
*[CVL]: Conseil de Vie Lycéenne
*[Pix]: service public en ligne pour évaluer, développer et certifier ses compétences numériques
*[MDL]: Maison des Lycéens
*[CESC]: Le comité d'éducation à la santé et à la citoyenneté
*[CPE]: Conseillé Principal d'Éducation
*[TP]: Travaux pratiques
