# EPS

Dans la continuité du collège, l'EPS a pour finalité de former par la pratique scolaire des activités physiques sportives et artistiques un citoyen cultivé, lucide et autonome, physiquement et socialement éduqué.

## 1] Les horaires

- Le lycée Aubrac propose un enseignement **obligatoire** de l'EPS pour tous les élèves de la seconde à la terminale de deux heures par semaine.
- Il propose également un enseignement **optionnel** pour l'ensemble des 3 niveaux de classe de trois heures par semaine sur un créneau situé en fin de journée (15 h / 18 h).
- Il est également possible de pratiquer au sein de **l'Association Sportive** des activités le plus souvent proposées entre midi et quatorze heure sur la base du volontariat en loisir, ou en compétition dans ce cas quelques mercredis après midi.

![Crosstraining, 2021](cross_training_2021.jpg) Crosstraining, 2021

## II] Les activités proposées en enseignement obligatoire

- En 2^nde^, nous proposons un ensemble de 4 activités qui respecte les textes en vigueur, à savoir : la course en durée, le badminton, la danse et l'escalade.

- En 1ère et terminale, les élèves ont un choix relatif de **menus** de trois activités respectant toujours les textes en vigueur et qui permettent aux élèves de découvrir de nouvelles activités, renouer avec des activités pratiquées au collège ou en club. Cette année, nous proposions par exemple un ensemble avec le badminton, l'acrosport et l'athlétisme ou bien un autre avec du handball, de la natation de course et de l'escalade ou enfin du _crosstraining_, du relai/vitesse et du rugby.
 L'évaluation se passe en contrôle continu, elle prend en compte des attendus en terme d'efficacité motrice, de rôles sociaux ou encore de progrès en fonction des niveaux de classe. Elle permet à l'élève de mesurer ses progrès, de prendre en compte son investissement et sa motivation.

## III] Les activités proposées en enseignement optionnel

Cette option vise un prolongement de l'enseignement obligatoire, elle est à destination d'un public sportif, motivé et intéressé par le Sport avec un grand S. L'objectif est de vivre différentes formes d'expériences sportives individuelles et collectives ; en conservant un perfectionnement dans nos activités historiques, le Badminton et le Handball où nous sommes chaque année champions départementaux, tout en s'ouvrant à d'autres sports comme le _crosstraining_, le football/Futsal, l'Athlétisme, le Mini Tennis, la pétanque... Sur chaque niveau de classe, des projets et thèmes d'étude (Santé, Métiers du sport, Environnement et Développement durable, Prévention et Protection des risques...) serviront de points d'appuis à la pratique des activités, **la finalité étant pour l'élève d'affiner la construction de son projet personnel d'orientation surtout si celui-ci est en rapport avec le Sport**.
 
## Pourquoi choisir cette Option ?

- Pour acquérir une expérience sportive plus approfondie.
- Pour pratiquer trois heures de plus dans la semaine, intégrées dans l'emploi du temps, sur place et découvrir des activités variées et multiples ou me perfectionner.
- Pour développer une réflexion ciblée afin d'aider l'élève dans son projet d'orientation et faciliter l'accès au Post bac en rapport avec les métiers du sport.
- Remplir sa boîte à souvenirs grâce aux voyages scolaires, aux compétitions sportives et aux moments sports.

![planing](planing.jpg)

--8<-- "includes/abbreviations.md"
