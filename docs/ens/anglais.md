# Anglais

Dans la continuité du collège, l’enseignement d’anglais en seconde vise à développer les cinq compétences : compréhension écrite et orale et expression écrite et orale (en continu et en interaction).

## Volume horaire

Les élèves bénéficient de 3 h d’anglais LVA par semaine.

## Programme

Les thèmes abordés sont puisés dans l’un des 8 axes au programme :

1. Vivre entre générations
2. Le monde du travail
3. La ville
4. La représentation de soi par rapport à autrui
5. Sports et société
6. La création et le rapport aux arts
7. Sauver la planète
8. Le passé dans le présent ; penser les futurs possibles.

Les documents étudiés sont variés (articles, chansons, films, reportages, textes littéraires, photos…) La première séquence fait le lien collège-lycée et permet d’évaluer les élèves. Une mise à niveau est souvent nécessaire.

En cours d’année, les élèves qui le souhaitent ont la possibilité de participer à divers projets tels que des voyages ou des sorties.  

En plus des 3 heures d'enseignement officiel et sur demande (mais les places sont limitées), les élèves peuvent s'inscrire en section européenne anglais (1 heure par semaine) afin d'améliorer leurs compétences linguistiques, de bénéficier d'une ouverture culturelle et de travailler différemment (en groupes et par projets). Ils bénéficieront aussi de 2 heures supplémentaires d'anglais en DNL Histoire-Géographie, soit un total de 6 heures d'anglais par semaine. La progression de l'anglais en section européenne suit celle de la DNL.

En Terminale, ces élèves pourront passer la certification de Cambridge (le CEC), équivalent au « _Cambridge First for Schools_ » niveau intermédiaire (ou B2 du CECRL), qui est valable à vie et reconnue par des milliers d’organisations dans le monde entier.

## Galerie de photos

Photos, par l'équipe d'anglais, licence libre

### _Tower Bridge_

![](anglais/1.JPG)

### _Tower Bridge_

![](anglais/2.JPG)

### _Buckingham Palace_

![](anglais/3.JPG)

### _The Gold State Coach at the Royal Mews_

![](anglais/4.JPG)

### _The Thames and Saint Paul's Cathedral_

![](anglais/5.JPG)

### _London Beach_

![](anglais/6.JPG)

### _London Skyline_

![](anglais/7.JPG)


## Présentation de la spécialité LLCE

par l'équipe des enseignants

<div><iframe src="LLCE.pdf" width="100%" height="700px"></div>


--8<-- "includes/abbreviations.md"
