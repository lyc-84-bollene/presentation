# ATRIUM

[ATRIUM](https://www.atrium-sud.fr/) est l'Environnement Numérique de Travail (ENT) du lycée, il permet

- de se connecter automatiquement à `pronote`,
- d'avoir accès à un espace de stockage personnel de travail,
- d'avoir accès à la messagerie interne du lycée,
- d'avoir accès aux sites collaboratifs des enseignants,
- d'avoir accès aux manuels en ligne,
- et bien d'autres choses encore.

--8<-- "includes/abbreviations.md"
