# Intendance

## Restauration scolaire

### Ouverture

|Lundi|Mardi|Jeudi|Vendredi|
|:---:|:---:|:---:|:------:|

Le restaurant scolaire est ouvert les lundis, mardis, jeudis et vendredis.

### Inscription 3 ou 4 jours

Selon l'emploi du temps de votre enfant, vous pouvez l'inscrire 3 ou 4 jours par semaine.

> **Le choix se fait la première quinzaine de septembre**.

### Coût

Le paiement est un forfait trimestriel, à titre *indicatif* la demi-pension pour l'année scolaire 2022-2023 s'élève à

- 503 € pour un demi-pensionnaire 4 jours ;
- 390 € pour un demi-pensionnaire 3 jours.

Le premier trimestre 2023-2024 (septembre - décembre 2023) s'élèvera à :

- 199.60 € pour un demi-pensionnaire 4 jours ;
- 149.10 € pour un demi-pensionnaire 3 jours.

Si votre enfant est externe il peut manger au restaurant scolaire en achetant un repas à 4.20 € auprès du secrétariat d'intendance. Il peut aussi être externe et prendre régulièrement un ou deux repas par semaine au restaurant scolaire en fonction de son emploi du temps.

### Paiement

Pour le paiement vous avez le choix entre :

- Prélèvement automatique : chaque trimestre est réglé en trois échéances.
- Paiement en une seule fois par chèque, carte bancaire ou espèces au secrétariat d'intendance.
- Paiement sécurisé par CB via Atrium ; vous pouvez régler en plusieurs fois chaque trimestre, c'est vous qui décidez. Seule obligation, la somme totale doit être réglée pour la dernière semaine du trimestre.

### Démarche qualité

Les repas du service de restauration sont préparés et élaborés sur place par _l'équipe de cuisine de l'établissement_. Une _attention particulière_ est accordée pour l'achat d'approvisionnements de _qualité_ issus notamment des circuits courts et de l'agriculture paysanne _non intensive_.

## Bourses nationales lycée

À ce jour, nous ne connaissons pas encore les modalités de la campagne de bourses pour septembre 2023.

> Attention si vous ne faites pas le dossier durant la campagne de bourses, votre enfant ne pourra pas être boursier pour l'année scolaire 2023-2024, **il est impossible de faire une demande de bourses nationales en lycée quand la campagne est clôturée**.

--8<-- "includes/abbreviations.md"
