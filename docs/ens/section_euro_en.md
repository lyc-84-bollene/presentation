# La section européenne HG en anglais

**HG** : **H**istoire-**G**éographie

**DNL** : **D**iscipline **N**on **L**inguistique

## Qu'est-ce que c'est ?

C'est une option ouverte depuis 2009 et proposée de la 2^de^ à la T^ale^ au Lycée Lucie Aubrac. Elle a pour objet l'obtention de la « mention européenne » sur le diplôme du baccalauréat, véritable plus-value pour le post-bac, et de manière plus générale l'ouverture européenne et culturelle, tout en proposant des méthodes de travail qui mettent en avant l'oral et le travail de groupe.

## Comment cela fonctionne-t-il ?

Les **élèves** de la section européenne sont **recrutés sur dossier** (places limitées) et répartis sur plusieurs classes, mais **regroupés par niveau pour les cours de DNL** Histoire Géographie en langue anglaise (2 h par semaine) et pour 1 h d'anglais. Ces 3 h s'ajoutent à l'horaire classique de LV anglais et d'histoire géographie de 2^de^.

Les élèves peuvent poursuivre cette option en 1^ère^ et en Terminale, ce qui créé un fort sentiment de cohésion entre les élèves de la section européenne, qui se suivent pendant 3 ans, même s'ils appartiennent à des classes différentes.

Certains points du programme d'histoire géographie sont précisés par le biais de sources anglophones, et des projets spécifiques, moins ciblés sur le programme d'histoire géographie et privilégiant le travail sur les médias, sont menés.

## Évaluation au baccalauréat

### À l'écrit

Au baccalauréat, la note sanctionnant la scolarité de l'élève dans sa section au cours de la classe terminale compte pour 20% de la note globale. Elle reflète le travail effectué en langue étrangère dans la DNL et elle est attribuée par le professeur de la discipline non linguistique en liaison avec le professeur de langue.
Elle prend en compte : la participation au travail oral dans la classe, la qualité de certains travaux réalisés au cours de l'année (brefs comptes rendus de lecture, commentaires de documents, productions personnelles et de groupe, etc.), la maitrise de la langue dans une situation de communication.

### À l'oral

Les élèves de section européenne passent également en fin de T^ale^ une épreuve orale qui compte pour 80% de la note globale. L'épreuve dure 20 minutes (précédées d'un temps égal de préparation).

L'évaluation est assurée par deux professeurs, l'un d'anglais, l'autre de DNL Histoire-Géographie en langue anglaise.

L'épreuve orale compte deux parties :

- Dans la 1^ère^ partie, l'élève travaille sur des documents inconnus en anglais (discours, extraits de presse écrite, documents iconographiques...).

- La deuxième partie de l'épreuve consiste en un entretien, conduit en anglais, qui porte sur les travaux et activités effectués au cours des trois années, dans le cadre de la section.

Pour obtenir la « mention européenne » sur le diplôme du baccalauréat, il faut obtenir une note supérieure ou égale à 12/20 en LV1 anglais, et une note supérieure ou égale à 10/20 à l'épreuve orale spécifique décrite ci-dessus.


--8<-- "includes/abbreviations.md"
