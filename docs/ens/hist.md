# :scroll: Histoire-Géographie, Éducation Civique :earth_africa:

## :timer_clock: Horaires

- Au lycée, l'**Histoire-Géographie** et l'**Enseignement Moral et Civique** (ou *EMC*) sont deux enseignements *distincts*.
- Toutes les classes ont dans leur emploi du temps plusieurs heures d'Histoire-Géographie (entre 1 h 30 et 3 h) et une demi-heure hebdomadaire d'EMC, de la Seconde à la Terminale.
- En classe de **Première et de Terminale générale**, les élèves peuvent aussi choisir, en plus de l'Histoire-Géographie, un *enseignement de spécialité* intitulé **Histoire-Géographie-Géopolitique-Science Politique**.

    - L'horaire de spécialité est de 4 heures hebdomadaires en Première et 6 heures hebdomadaires en Terminale.

|    Seconde    | Première Générale |  Première STMG  | Terminale Générale | Terminale STMG  |
|:-------------:|:-----------------:|:---------------:|:------------------:|:---------------:|
| 3h + 0,5h EMC |   3h + 0,5h EMC   | 1,5h + 0,5h EMC |   3h + 0,5h EMC    | 1,5h + 0,5h EMC |
|               | Spécialité HGGSP  |                 |  Spécialité HGGSP  |                 |
|               |        4h         |                 |         6h         |                 |

## Programmes de la classe de Seconde

Faites défiler le diaporama ci-dessous :arrow_down:

<script async class="speakerdeck-embed" data-id="e32fa5af0dd040f2ba843ad410addf8b" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

## Présentation de la spécialité

<script async class="speakerdeck-embed" data-id="26c50c0e069f4417bf22e2532dba42ef" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

### Un exemple de contenus abordés en première HGGSP :pencil:

<div class="padlet-embed" style="border:1px solid rgba(0,0,0,0.1);border-radius:2px;box-sizing:border-box;overflow:hidden;position:relative;width:100%;background:#F4F4F4"><p style="padding:0;margin:0"><iframe src="https://padlet.com/embed/uxmhqilw7apx" frameborder="0" allow="camera;microphone;geolocation" style="width:100%;height:608px;display:block;padding:0;margin:0"></iframe></p><div style="padding:8px;text-align:right;margin:0;"><a href="https://padlet.com?ref=embed" style="padding:0;margin:0;border:none;display:block;line-height:1;height:16px" target="_blank"><img src="https://padlet.net/embeds/made_with_padlet.png" width="86" height="16" style="padding:0;margin:0;background:none;border:none;display:inline;box-shadow:none" alt="Fait avec Padlet"></a></div></div>

--8<-- "includes/abbreviations.md"
